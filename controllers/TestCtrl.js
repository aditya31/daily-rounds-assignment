const express = require('express');
var mongoose = require('mongoose');
var async = require('async');

var route = express.Router();


TestModel = require('../Models/TestModel.js')
QuestionsModel = require('../Models/QuestionsModel.js')
AnswerKeyModel = require('../Models/AnswerKeyModel.js')
TestDifficultyModel = require('../Models/TestDifficultyModel.js')

route.get("/:testId/user/:userId", function (req, res) {
  var testId = req.params.testId;
  var userId = req.params.userId;
  TestModel.findById(testId).lean().exec().then(function (test) {
    var fns = [
      TestSubmissionModel.findOne({
        userId: userId,
        testId: testId
      }).exec(),
      QuestionsModel.findOne({
        testId: testId
      }).exec()
    ]

    Promise.all(fns).then(function (results) {
      
        if (results[0]) {
          test.analytics = results[1];
        }

        if (results[1]) {
          test.questions = results[2].questions;
        }
        res.json(test);
      
    }, function(err){
        res.status(500);
        res.json({
          err: err.message
        });
    })

  }, function(err){
    if (err) {
      res.status(500);
      res.json({
        err: err.message
      });
      return;
    }
  })
})

route.get("/index/:userId", function (req, res) {
  var userId = req.params.userId;
  TestModel.find({}).lean().exec().then(function (tests) {
    async.eachSeries(tests, function (test, next) {
      TestSubmissionModel.findOne({
        userId: userId
      }, function (err, submissions) {
        if (err) {
          next(err);
          return;
        }
        if (submissions) {
          test.analytics = {
            totalScore : submissions.totalScore,
            totalPercentile : submissions.totalPercentile,
            sub_score : submissions.sub_score,
            sub_percentile : submissions.sub_percentile,
            rank : submissions.rank,
            submitted_on : submissions.submitted_on
          };
        }
        next();
      });
    }, function (err) {
      if (err) {
        res.status(500);
        res.json({
          err: err.message
        });
      } else {
        res.json(tests);
      }
    });
  }, function(err){
    if (err) {
      res.status(500);
      res.json({
        err: err.message
      });
      return;
    }
  })
});

route.get("/gen", function (req, res) {
  Utils.createRandomTest(function () {
    res.json({
      done: true
    });
  })
});



// Function which will run once the test ends
function EndTest(testId, cb) {
  var fns = [
    async.apply(updateWeightedScore, testId),
    async.apply(updateAnalytics, testId)
  ];

  async.series(fns, function (err) {
    cb(err);
  })

}


function updateWeightedScore(testId, cb) {
  var fns = [
    TestSubmissionModel.find({
      testId: testId
    }).exec(),
    TestDifficultyModel.findOne({
      testId: testId
    }).exec()
  ];
  Promise.all(fns).then(function (results) {

    var submissions = results[0];
    var difficultyDoc = results[1].questions;
    var totalSubmissionCount = submissions.length;
    async.eachSeries(submissions, function (sub, next) {
      var weight = 0;
      for (q in sub.answers) {
        if (sub.answers[q].correct) {
          weight += totalSubmissionCount - difficultyDoc[q];
        }
      }

      var weightedScore = sub.totalScore + ((weight / totalSubmissionCount) * sub.totalScore);

      TestSubmissionModel.update({
        testId: testId,
        userId: sub.userId
      }, {
        weightedScore: weightedScore
      }, {}, function () {
        next();
      })

    }, function () {
      cb();
    })

  }, function (err) {
    cb(err);
  })
}

function updateAnalytics(testId, cb) {
  var fns = [
    TestSubmissionModel.count({
      testId: testId
    }).exec(),
    TestModel.findById(testId).exec()
  ]
  Promise.all(fns).then(function (results) {
    var totalSubCount = results[0];
    var testDef = results[1];

    TestSubmissionModel.find({
      testId: testId
    }, function (err, submissions) {
      async.eachSeries(submissions, function (sub, next) {
        
        var aggregate = TestSubmissionModel.aggregate(
          [{
              $match: {
                testId: mongoose.Types.ObjectId(testId),
                totalScore: sub.totalScore,
                weightedScore: {
                  $lt: sub.weightedScore
                }
              }
            },
            {
              $sort: {
                weightedScore: -1
              }
            }
          ]
        );

        var fns2 = [
          TestSubmissionModel.count({ testId: testId, totalScore: { $lt: sub.totalScore }}).exec(),
          aggregate.exec()
        ];

        Promise.all(fns2).then(function (results) {
          var uRank = totalSubCount - (results[0] + results[1].length);
          var totalPercentile = ((results[0] + results[1].length)/totalSubCount) * 100;

          var fns3 =[];
          var s = [];

          var updates = {};
          updates.rank = uRank;
          updates.totalPercentile = totalPercentile;
          updates.sub_percentile = {};

          for(subject in sub.sub_score){
            console.log('subject: ', subject);
            var query = { testId: testId};
            query["sub_score."+subject] = { $lt: sub.sub_score[subject]};
            console.log('query: ', query);
            fns3.push(TestSubmissionModel.count(query).exec());
            s.push(subject);
          }

          Promise.all(fns3).then(function(results){
            results.forEach(function(result, idx){
              updates.sub_percentile[s[idx]] = (result/totalSubCount) * 100;
            })

            TestSubmissionModel.update({testId: testId,userId: sub.userId }, updates, {}, function (err) {
              next()
            })

          },function(err){
            cb(err)
          })
        }, function (err) {
          cb(err);
        })
      }, function(err){
        cb(err);
      })
    })

  }, function (err) {
    cb(err);
  })
}

module.exports = route;