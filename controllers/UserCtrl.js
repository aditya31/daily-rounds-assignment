const express = require('express');
var async = require('async');

var route = express.Router();

TestModel = require('../Models/TestModel.js')
UserModel = require('../Models/UserModel.js')
QuestionsModel = require('../Models/QuestionsModel.js')
AnswerKeyModel = require('../Models/AnswerKeyModel.js')
TestSubmissionModel = require('../Models/TestSubmissionModel.js');
TestDifficultyModel = require('../Models/TestDifficultyModel.js')



route.post("/", function(req, res){
  var newUser = new UserModel(req.body);
  newUser.save(function () {
    res.json(req.body);
  })
})

route.post("/:userId/TestSubmission/:testId", function(req, res){

  var userId = req.params.userId;
  var testId = req.params.testId;
  var submittedAnswers = req.body.answers;

  var mock = false;
  TestModel.findById(testId, function(err, test){
    var cDate = new Date();
    if(new Date(test.endTime).getTime() < cDate.getTime()){
      mock = true;  
    }
    saveUserSubmission(userId, testId, submittedAnswers, mock,function(err, submittedTest){
      if(err){
        res.status(500);
        res.json({err : err.message})
      } else{
        res.json(submittedTest);
      }
    })
  })

});


function saveUserSubmission(userId, testId, submittedAnswers, mock, cb){

  var submittedTest = {};
  submittedTest.userId = userId;
  submittedTest.testId = testId;  
  submittedTest.totalScore = 0;
  submittedTest.sub_score = {};

  var asyncPromises = [
    TestSubmissionModel.findOne({testId : testId, userId : userId}).exec(),
    QuestionsModel.findOne({testId : testId}).exec(),
    AnswerKeyModel.findOne({testId : testId}).exec()
  ]

  Promise.all(asyncPromises).then(function(results){
    if(results[0]){
      cb(new Error("TEST_ALREADY_SUBMITTED"));
      return;
    }
    var questionsDef = results[1];
    var answerKey = results[2];
    for(q in questionsDef.questions){
      var award;
      if(submittedAnswers[q]){
        if(submittedAnswers[q].selectedOption === answerKey.answers[q]){
          award = 5;
          submittedAnswers[q].correct = true;
        } else{
          award = -1;
          submittedAnswers[q].correct = false;
        }

        submittedTest.totalScore += award;
        if(submittedTest.sub_score[questionsDef.questions[q].sub_id] === undefined){
          submittedTest.sub_score[questionsDef.questions[q].sub_id] = 0
        }
        submittedTest.sub_score[questionsDef.questions[q].sub_id] += award;

      }
    }

    submittedTest.answers = submittedAnswers;

    var newSubmission = new TestSubmissionModel(submittedTest);
    newSubmission.save(function(err){
      if(err){
        cb(err);
        return;
      }
      var updates = {"$inc" : {}};
      var correctQ = 0;
      for(q in submittedAnswers){
        if(submittedAnswers[q].correct){
          updates["$inc"]["questions."+q] = 1;
          correctQ++;
        }
      }

      var fns = [];

      if(correctQ > 0){
        fns.push(TestDifficultyModel.findOneAndUpdate({testId : testId}, updates, {new : true}).exec());
      }

      Promise.all(fns).then(function(results){
        if(mock){
          getMockRank(testId, userId, function(){
            cb(null, submittedTest);
          })
        } else{
          cb(null, submittedTest);
        }
      })


    })

  })
}


function getMockRank(testId, userId, cb){
  var totalSubCount;
  var fns = [
    TestSubmissionModel.findOne({testId : testId, userId : userId}).exec(),
    TestDifficultyModel.findOne({testId : testId}).exec()
  ];

  Promise.all(fns).then(function(results){
    userSubmission = results[0];
    TestDifficulty = results[1].questions;
    var aggregate = TestSubmissionModel.aggregate(
      [{
          $match: {
            testId: mongoose.Types.ObjectId(testId),
            totalScore: userSubmission.totalScore,
            weightedScore: {
              $lt: userSubmission.weightedScore
            }
          }
        },
        {
          $sort: {
            weightedScore: -1
          }
        }
      ]
    );

    var fns2 = [
      TestSubmissionModel.count({testId : testId}).exec(),
      TestSubmissionModel.count({testId : testId, totalScore : {$lt : userSubmission.totalScore}}).exec(),
      aggregate.exec()
    ];

    Promise.all(fns2).then(function (results) {
      var uRank = results[0] - (results[1] + results[2].length);
      var totalPercentile = ((results[1] + results[2].length)/results[0]) * 100;
      var totalSubCount = results[0];
      var fns3 =[];
      var s = [];

      var updates = {};
      updates.rank = uRank;
      updates.totalPercentile = totalPercentile;
      updates.sub_percentile = {};

      for(subject in userSubmission.sub_score){
        var query = { testId: testId};
        query["sub_score."+subject] = { $lt: userSubmission.sub_score[subject]};
        fns3.push(TestSubmissionModel.count(query).exec());
        s.push(subject);
      }

      Promise.all(fns3).then(function(results){
        results.forEach(function(result, idx){
          updates.sub_percentile[s[idx]] = (result/totalSubCount) * 100;
        })

        console.log('updates: ', updates);

        TestSubmissionModel.update({testId: testId,userId: userId }, updates, {}, function (err) {
          cb()
        })

      },function(err){
        cb(err)
      })
    }, function (err) {
      cb(err);
    })

  }, function(err){
    cb(err);
  })

}


module.exports = route;



