mongoose = require('mongoose')
var Schema = mongoose.Schema;

var UserSchema = new Schema(
    {
        name : {
            type : String,
            required : true
        },
        phoneNumber : {
            type : String,
            required : true
        }
    },
    {
        versionKey : false
    }
);

module.exports = mongoose.model('User' , UserSchema);