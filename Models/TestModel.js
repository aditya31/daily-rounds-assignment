mongoose = require('mongoose')
var Schema = mongoose.Schema;

var TestSchema = new Schema(
    {
        title : {
            type : String,
            required : true
        },
        duration : {
            type : Number,
            required : true
        },
        startTime : {
            type : Date,
            required : true
        },
        endTime : {
            type : Date,
            required : true
        }
    },
    {
        versionKey : false
    }
);

module.exports = mongoose.model('Test' , TestSchema);