mongoose = require('mongoose')
var Schema = mongoose.Schema;

var TestSubmissionSchema = new Schema(
    {
        userId : {
            type : String,
            required : true
        },
        testId : {
            type : Schema.Types.ObjectId,
            ref : 'Test',
            required : true
        },
        answers : {
            type : {},
            required : false
        },
        totalScore : {
            type : Number,
            required : false
        },
        totalPercentile : {
            type : Number,
            required : false
        },
        weightedScore : {
            type : Number,
            required : false
        },
        sub_score : {
            type : {},
            required : false
        },
        sub_percentile : {
            type : {},
            required : false
        },
        
        submitted_on :{
            type : Date,
            required : false
        }
    },
    {
        versionKey : false
    }
);

TestSubmissionSchema.pre('save' , function (next) {
    this.submitted_on = new Date();
    next();
});

module.exports = mongoose.model('TestSubmission' , TestSubmissionSchema);