mongoose = require('mongoose')
var Schema = mongoose.Schema;

var TestDifficultyModel = new Schema(
    {
        testId : {
            type : Schema.Types.ObjectId,
            ref : 'Test',
            required : true
        },
        questions : {
            type : {},
            required : true
        }
    },
    {
        versionKey : false
    }
);

module.exports = mongoose.model('TestDiff' , TestDifficultyModel);