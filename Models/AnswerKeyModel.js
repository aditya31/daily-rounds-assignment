mongoose = require('mongoose')
var Schema = mongoose.Schema;

var AnswerKeySchema = new Schema(
    {
        testId : {
            type : Schema.Types.ObjectId,
            ref : 'Test',
            required : true
        },
        answers : {
            type : {},
            required : true
        }
    },
    {
        versionKey : false
    }
);

module.exports = mongoose.model('AnswerKey' , AnswerKeySchema);