var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var TestCtrl = require('./controllers/TestCtrl.js');
var UserCtrl = require('./controllers/UserCtrl.js');


app.use("/tests",TestCtrl);
app.use("/users",UserCtrl);

app.get('/', function (req, res) {
  res.json({
  	timeStamp : new Date()
  })
})

mongoose.connect("mongodb://localhost:27017/dailyRounds" , {useNewUrlParser: true}).connection;

const PORT = process.env.PORT || process.argv[2] || 3121;

app.listen(PORT , function () {
    console.log('Server is running on port :' , PORT);
});